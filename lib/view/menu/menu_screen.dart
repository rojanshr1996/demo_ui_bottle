import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:animate_icons/animate_icons.dart';

class MenuScreen extends StatefulHookWidget {
  final List<String> headerTitles;
  final int currentValue;
  const MenuScreen({
    super.key,
    required this.headerTitles,
    this.currentValue = 1,
  });

  @override
  State<MenuScreen> createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  late AnimateIconController c1;

  @override
  void initState() {
    super.initState();
    c1 = AnimateIconController();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(50.ms, () {
      if (c1.isStart()) {
        c1.animateToEnd();
      }
    });
    ValueNotifier<int> curr = useState(widget.currentValue);
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.darkPurpleColor,
        onPressed: () {},
        child: AnimateIcons(
          controller: c1,
          startIcon: Icons.add,
          endIcon: Icons.close,
          onStartIconPress: () {
            return true;
          },
          onEndIconPress: () {
            Utilities.closeActivity(context);
            return true;
          },
          duration: const Duration(milliseconds: 350),
          startIconColor: AppColors.whiteColor,
          endIconColor: AppColors.whiteColor,
          clockwise: false,
        ),
      ),
      body: SizedBox(
        height: Utilities.screenHeight(context),
        width: Utilities.screenWidth(context),
        child: ListView.builder(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top + 30.0),
            physics: const BouncingScrollPhysics(),
            itemCount: widget.headerTitles.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 50.0),
                child: Hero(
                  tag: widget.headerTitles[index],
                  flightShuttleBuilder: (
                    BuildContext flightContext,
                    Animation<double> animation,
                    HeroFlightDirection flightDirection,
                    BuildContext fromHeroContext,
                    BuildContext toHeroContext,
                  ) {
                    return SingleChildScrollView(
                      child: toHeroContext.widget,
                    );
                  },
                  child: Material(
                    type: MaterialType.transparency,
                    child: widget.headerTitles[index] == "you"
                        ? Material(
                            type: MaterialType.transparency,
                            child: SizedBox(
                              // height: 100,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    height: curr.value == index ? 80 : 60,
                                    width: curr.value == index ? 80 : 60,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle, border: Border.all(color: AppColors.greyColorShadow)),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Image.network(
                                        "https://fastly.picsum.photos/id/237/536/354.jpg?hmac=i0yVXW1ORpyCZpQ-CknuyV-jbtU7_x9EBQVhvT5aRr0",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ).animate().fade(duration: const Duration(milliseconds: 350)),
                                  const SizedBox(height: 10),
                                  Text("YOU",
                                      style: TextStyle(
                                          fontWeight: curr.value == index ? bold : medium,
                                          fontSize: curr.value == index ? 16 : 12))
                                ],
                              ),
                            ),
                          )
                        : Material(
                            type: MaterialType.transparency,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                FittedBox(
                                  fit: BoxFit.contain,
                                  child: Container(
                                    height: curr.value == index ? 80 : 60,
                                    width: curr.value == index ? 80 : 60,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        color: curr.value == index
                                            ? widget.headerTitles[index] == "trending"
                                                ? AppColors.pinkColor
                                                : widget.headerTitles[index] == "health"
                                                    ? AppColors.lightGreenColor
                                                    : widget.headerTitles[index] == "music"
                                                        ? AppColors.skyBlueColor
                                                        : AppColors.greyColorShadow
                                            : AppColors.greyColorShadow,
                                        width: curr.value == index ? 2.5 : 1,
                                      ),
                                    ),
                                    child: Center(
                                      child: widget.headerTitles[index] == "trending"
                                          ? Icon(Icons.trending_up,
                                              size: curr.value == index ? 40 : 28,
                                              color: curr.value == index
                                                  ? AppColors.darkBlueColor
                                                  : AppColors.fadedBlueColor)
                                          : widget.headerTitles[index] == "health"
                                              ? Icon(Icons.favorite_outline,
                                                  size: curr.value == index ? 40 : 28,
                                                  color: curr.value == index
                                                      ? AppColors.darkBlueColor
                                                      : AppColors.fadedBlueColor)
                                              : widget.headerTitles[index] == "music"
                                                  ? Icon(Icons.headphones,
                                                      size: curr.value == index ? 40 : 28,
                                                      color: curr.value == index
                                                          ? AppColors.darkBlueColor
                                                          : AppColors.fadedBlueColor)
                                                  : widget.headerTitles[index] == "sports"
                                                      ? Icon(Icons.sports_basketball_outlined,
                                                          size: curr.value == index ? 40 : 28,
                                                          color: curr.value == index
                                                              ? AppColors.darkBlueColor
                                                              : AppColors.fadedBlueColor)
                                                      : Icon(Icons.chrome_reader_mode_outlined,
                                                          size: curr.value == index ? 40 : 28,
                                                          color: curr.value == index
                                                              ? AppColors.darkBlueColor
                                                              : AppColors.fadedBlueColor),
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 10),
                                FittedBox(
                                  fit: BoxFit.contain,
                                  child: Text(widget.headerTitles[index].toUpperCase(),
                                      style: TextStyle(
                                          fontWeight: curr.value == index ? bold : medium,
                                          fontSize: curr.value == index ? 16 : 12)),
                                ),
                              ],
                            ),
                          ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
