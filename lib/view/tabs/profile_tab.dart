import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/profile_widgets/name_widget.dart';
import 'package:bottle_demo_ui/res/components/rounded_icon_widget.dart';
import 'package:bottle_demo_ui/res/components/title_widget.dart';
import 'package:bottle_demo_ui/res/components/profile_widgets/section_text_widget.dart';
import 'package:bottle_demo_ui/res/route/routes.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:provider/provider.dart';

class ProfileTab extends StatefulWidget {
  const ProfileTab({super.key});

  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.darkPurpleColor,
      body: Consumer<HomeProvider>(
        builder: (context, homeProvider, _) {
          return SizedBox(
            height: Utilities.screenHeight(context),
            width: Utilities.screenWidth(context),
            child: homeProvider.isDetailLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : homeProvider.profileData == null
                    ? const TitleWidget(title: "No data")
                    : CustomScrollView(
                        physics: const BouncingScrollPhysics(),
                        slivers: [
                          SliverToBoxAdapter(
                            child: Stack(
                              children: [
                                ClipRRect(
                                  borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(80)),
                                  child: Image.network(
                                    height: Utilities.screenHeight(context) * 0.5,
                                    homeProvider.profileData?.response.image ?? "",
                                    fit: BoxFit.cover,
                                  ).animate().fade().slideX(
                                      duration: 500.ms,
                                      curve: Curves.decelerate,
                                      begin: -1,
                                      end: 0,
                                      delay: const Duration(milliseconds: 50)),
                                ),
                                Positioned(
                                  bottom: -0.5,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      height: Utilities.screenHeight(context) * 0.10,
                                      width: Utilities.screenWidth(context),
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(80)),
                                        gradient: AppColors.lightPurpleGradientPicture,
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 35,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: SizedBox(
                                      width: Utilities.screenWidth(context),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                                padding: const EdgeInsets.only(left: 40, right: 18),
                                                child: NameWidget(
                                                  meetupCount: homeProvider.profileData?.response.totalMeetups ?? 0,
                                                  name: homeProvider.profileData?.response.name ?? "",
                                                )),
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.symmetric(horizontal: 16),
                                            child: RoundedIconWidget(
                                              iconData: Icons.edit_outlined,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Card(
                              color: AppColors.darkPurpleColor,
                              shadowColor: AppColors.lightPurpleAccentColor,
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60))),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 18),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        ProfileIconWidget(
                                          iconData: Icons.email_outlined,
                                          iconTitle: "Messages",
                                          onPressed: () {
                                            Utilities.openNamedActivity(context, Routes.message);
                                          },
                                        ),
                                        Container(
                                          width: 1,
                                          height: Utilities.screenHeight(context) * 0.1,
                                          color: AppColors.greyColorShadow,
                                        ),
                                        ProfileIconWidget(
                                          iconData: Icons.notifications_outlined,
                                          iconTitle: "Notifications",
                                          onPressed: () {},
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ).animate().fade().slideX(
                                duration: const Duration(milliseconds: 400),
                                curve: Curves.decelerate,
                                begin: -1,
                                end: 0,
                                delay: const Duration(milliseconds: 50)),
                          ),
                          SliverToBoxAdapter(
                            child: SectionTextWidget(
                              title: "About Me",
                              content: "${homeProvider.profileData?.response.aboutMe}",
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: SectionTextWidget(
                              title: "Timeline",
                              content: "${homeProvider.profileData?.response.timeline}",
                            ),
                          ),
                        ],
                      ),
          );
        },
      ),
    );
  }
}

class ProfileIconWidget extends StatelessWidget {
  final IconData iconData;
  final VoidCallback? onPressed;
  final String iconTitle;
  final TextStyle? textStyle;
  const ProfileIconWidget({super.key, required this.iconData, this.onPressed, required this.iconTitle, this.textStyle});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: onPressed,
          child: Icon(
            iconData,
            color: AppColors.whiteColor,
            size: 50,
          ),
        ),
        Text(
          iconTitle,
          style: textStyle ?? const TextStyle(color: AppColors.lightPurpleAccentColor),
          textAlign: TextAlign.center,
        )
      ],
    );
  }
}
