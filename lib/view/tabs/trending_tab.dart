import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/tab_widgets/trending_list_item_widget.dart';

import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:bottle_demo_ui/view/trending/trending_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TrendingTab extends StatefulWidget {
  const TrendingTab({super.key});

  @override
  State<TrendingTab> createState() => _TrendingTabState();
}

class _TrendingTabState extends State<TrendingTab> {
  List<Color> colorList = [
    AppColors.pinkColor,
    AppColors.purpleColor,
    AppColors.darkPurpleColor,
    AppColors.lightPurpleColor,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightPurpleColor,
      body: Consumer<HomeProvider>(
        builder: (context, homeProvider, _) {
          return SizedBox(
            height: Utilities.screenHeight(context),
            width: Utilities.screenWidth(context),
            child: homeProvider.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : TrendingListItemWidget(
                    backgroundColor: colorList,
                    trendingList: homeProvider.homeData?.response.trending.data ?? [],
                    onPressed: (trendingData) {
                      Utilities.openActivity(
                        context,
                        TrendingDetailScreen(trendingData: trendingData),
                        transitionDuration: 500,
                        transitionBuilder: (_, Animation<double> animation, __, Widget child) {
                          return FadeTransition(opacity: animation, child: child);
                        },
                      );
                    },
                  ),
          );
        },
      ),
    );
  }
}
