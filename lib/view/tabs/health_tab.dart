import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/tab_widgets/health_list_item_widget.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HealthTab extends StatefulWidget {
  const HealthTab({super.key});

  @override
  State<HealthTab> createState() => _HealthTabState();
}

class _HealthTabState extends State<HealthTab> {
  List<Color> colorList = [
    AppColors.lightGreenColor,
    AppColors.greenAccentColor,
    AppColors.darkGreenColor,
    AppColors.lightPurpleColor,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightGreenColor.withOpacity(0.5),
      body: Consumer<HomeProvider>(
        builder: (context, homeProvider, _) {
          return SizedBox(
            height: Utilities.screenHeight(context),
            width: Utilities.screenWidth(context),
            child: homeProvider.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : HealthListItemWidget(
                    backgroundColor: colorList,
                    healthList: homeProvider.homeData?.response.health.data ?? [],
                    onPressed: (healthData) {},
                  ),
          );
        },
      ),
    );
  }
}
