import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/tab_widgets/music_list_item_widget.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MusicTab extends StatefulWidget {
  const MusicTab({super.key});

  @override
  State<MusicTab> createState() => _MusicTabState();
}

class _MusicTabState extends State<MusicTab> {
  List<Color> colorList = [
    AppColors.skyBlueColor,
    AppColors.blueColor,
    AppColors.darkBlueColor,
    AppColors.lightPurpleColor,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightPurpleColor,
      body: Consumer<HomeProvider>(
        builder: (context, homeProvider, _) {
          return SizedBox(
            height: Utilities.screenHeight(context),
            width: Utilities.screenWidth(context),
            child: homeProvider.isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : MusicListItemWidget(
                    backgroundColor: colorList,
                    musicList: homeProvider.homeData?.response.music.data ?? [],
                    onPressed: (healthData) {},
                  ),
          );
        },
      ),
    );
  }
}
