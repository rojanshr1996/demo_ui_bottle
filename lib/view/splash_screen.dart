import 'dart:developer';

import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/route/routes.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () => loadData(context));
  }

  loadData(BuildContext context) async {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    await homeProvider.getHomeData();
    await homeProvider.getDetailData();
    await homeProvider.getProfileData();
    await homeProvider.getMessageData();

    Future.delayed(const Duration(milliseconds: 750), () => Utilities.replaceNamedActivity(context, Routes.home));
  }

  @override
  Widget build(BuildContext context) {
    log(DateTime.now().toLocal().toIso8601String());
    return Scaffold(
      backgroundColor: AppColors.darkPurpleColor,
      body: Container(),
    );
  }
}
