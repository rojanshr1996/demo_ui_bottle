import 'package:bottle_demo_ui/model/home_model.dart';
import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/trending_detail_widgets/detail_content_widget.dart';
import 'package:bottle_demo_ui/res/components/trending_detail_widgets/detail_stack_widget.dart';
import 'package:bottle_demo_ui/res/components/trending_detail_widgets/maps_widget.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:bottle_demo_ui/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class TrendingDetailScreen extends StatefulWidget {
  final Datum trendingData;
  const TrendingDetailScreen({Key? key, required this.trendingData}) : super(key: key);

  @override
  State<TrendingDetailScreen> createState() => _TrendingDetailScreenState();
}

class _TrendingDetailScreenState extends State<TrendingDetailScreen> {
  List<String> names = [];

  @override
  void initState() {
    super.initState();

    if (widget.trendingData.members.isNotEmpty) {
      for (Member element in widget.trendingData.members) {
        names.add(element.name.name);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.darkPurpleColor,
      body: Consumer<HomeProvider>(
        builder: (context, homeProvider, _) {
          final detailData = homeProvider.detailData?.response;
          return SizedBox(
            height: Utilities.screenHeight(context),
            width: Utilities.screenWidth(context),
            child: homeProvider.isDetailLoading
                ? const Center(child: CircularProgressIndicator())
                : NestedScrollView(
                    physics: const BouncingScrollPhysics(),
                    headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                      return <Widget>[
                        SliverList(
                          delegate: SliverChildListDelegate(
                            [
                              Column(
                                children: [
                                  Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      DetailStackWidget(
                                        detailData: detailData!,
                                        onAccept: (isLiked) async {
                                          homeProvider.setOnChecked(!isLiked);
                                          return !isLiked;
                                        },
                                      ),
                                      Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Hero(
                                            tag: "Trending: ${widget.trendingData.date}",
                                            child: Material(
                                              type: MaterialType.transparency,
                                              borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(80)),
                                              child: Container(
                                                width: Utilities.screenWidth(context),
                                                decoration: const BoxDecoration(
                                                    color: AppColors.pinkColor,
                                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(80))),
                                                child: Padding(
                                                  padding: const EdgeInsets.symmetric(vertical: 40),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      const SizedBox(height: 10),
                                                      Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: [
                                                            BackButton(
                                                              onPressed: () {
                                                                Utilities.closeActivity(context);
                                                              },
                                                              color: AppColors.whiteColor,
                                                            ),
                                                            IconButton(
                                                              onPressed: () {},
                                                              icon: const Icon(
                                                                Icons.file_upload_outlined,
                                                                color: AppColors.whiteColor,
                                                              ),
                                                            )
                                                          ],
                                                        ).animate().fade(
                                                              duration: const Duration(milliseconds: 500),
                                                              delay: const Duration(milliseconds: 100),
                                                            ),
                                                      ),
                                                      const SizedBox(height: 15),
                                                      FittedBox(
                                                        fit: BoxFit.contain,
                                                        child: Padding(
                                                          padding: const EdgeInsets.symmetric(horizontal: 40),
                                                          child: Text(
                                                            widget.trendingData.title,
                                                            style: const TextStyle(
                                                                fontSize: 24,
                                                                fontWeight: bold,
                                                                color: AppColors.whiteColor),
                                                            softWrap: true,
                                                            maxLines: 2,
                                                            overflow: TextOverflow.ellipsis,
                                                          ),
                                                        ),
                                                      ),
                                                      const SizedBox(height: 15),
                                                      Padding(
                                                        padding: const EdgeInsets.symmetric(horizontal: 40),
                                                        child: names.isEmpty
                                                            ? const SizedBox.shrink()
                                                            : Row(
                                                                children: [
                                                                  Utils.buildStackedImages(
                                                                      membersList: widget.trendingData.members),
                                                                  const SizedBox(width: 10),
                                                                  Text(
                                                                    "${names[1]}, ${names[2]} & ${names.length - 2} others",
                                                                    style: TextStyle(
                                                                      fontSize: 12,
                                                                      fontStyle: FontStyle.italic,
                                                                      fontWeight: medium,
                                                                      color: AppColors.whiteColor.withOpacity(0.5),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ];
                    },
                    body: SizedBox(
                      height: Utilities.screenHeight(context),
                      width: Utilities.screenWidth(context),
                      child: SingleChildScrollView(
                        physics: const BouncingScrollPhysics(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            detailData == null
                                ? const SizedBox.shrink()
                                : DetailContentWidget(
                                    iconData: Icons.access_time,
                                    content: [
                                      Text(
                                        DateFormat.yMMMEd().format(detailData.date),
                                        style: const TextStyle(fontWeight: bold, color: AppColors.whiteColor),
                                        softWrap: true,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      const SizedBox(height: 8),
                                      Text(
                                        DateFormat.jm().format(detailData.date),
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: medium,
                                          color: AppColors.lightPurpleAccentColorShadow,
                                        ),
                                        softWrap: true,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      const SizedBox(height: 8),
                                      Text(
                                        detailData.repeat,
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: medium,
                                          color: AppColors.lightPurpleAccentColorShadow,
                                        ),
                                        softWrap: true,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                            detailData == null
                                ? const SizedBox.shrink()
                                : DetailContentWidget(
                                    iconData: Icons.access_time,
                                    content: [
                                      Text(
                                        detailData.location,
                                        style: const TextStyle(fontWeight: bold, color: AppColors.whiteColor),
                                        softWrap: true,
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      const SizedBox(height: 12),
                                      Text(
                                        detailData.address,
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: medium,
                                          color: AppColors.lightPurpleAccentColorShadow,
                                        ),
                                        softWrap: true,
                                        maxLines: 3,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      const SizedBox(height: 12),
                                      Container(
                                        height: 130,
                                        width: Utilities.screenWidth(context),
                                        decoration: BoxDecoration(
                                          color: AppColors.lightPurpleAccentColorShadow,
                                          borderRadius: const BorderRadius.only(
                                            topRight: Radius.circular(80),
                                            bottomLeft: Radius.circular(80),
                                          ),
                                        ),
                                        child: const ClipRRect(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(80),
                                              bottomLeft: Radius.circular(80),
                                            ),
                                            child: MapsWidget()),
                                      ),
                                    ],
                                  ),
                            detailData == null
                                ? const SizedBox.shrink()
                                : DetailContentWidget(
                                    iconData: Icons.credit_card,
                                    content: [
                                      Text(
                                        detailData.cost,
                                        style: const TextStyle(fontWeight: bold, color: AppColors.whiteColor),
                                        softWrap: true,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                            detailData == null
                                ? const SizedBox.shrink()
                                : DetailContentWidget(
                                    iconData: Icons.person,
                                    content: [
                                      Text(
                                        "Hosted by ${detailData.host}",
                                        style: const TextStyle(fontWeight: bold, color: AppColors.whiteColor),
                                        softWrap: true,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                            detailData == null
                                ? const SizedBox.shrink()
                                : DetailContentWidget(
                                    content: [
                                      Text(
                                        detailData.description,
                                        style: const TextStyle(
                                          fontWeight: medium,
                                          fontSize: 16,
                                          height: 1.5,
                                          color: AppColors.lightPurpleAccentColor,
                                        ),
                                        softWrap: true,
                                      ),
                                    ],
                                  ),
                            detailData == null
                                ? const SizedBox.shrink()
                                : DetailContentWidget(
                                    iconData: Icons.chat_bubble_outline,
                                    content: [
                                      const Text(
                                        "Live Chat",
                                        style: TextStyle(fontWeight: bold, color: AppColors.whiteColor),
                                        softWrap: true,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      const SizedBox(height: 12),
                                      Utils.buildStackedImages(
                                          membersList: detailData.members,
                                          length: detailData.members.length,
                                          size: 35,
                                          xShift: 8),
                                    ],
                                  ),
                          ],
                        ),
                      ),
                    ),
                  ),
          );
        },
      ),
    );
  }
}
