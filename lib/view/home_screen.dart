import 'dart:developer';

import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:bottle_demo_ui/view/menu/menu_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulHookWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  // List<String> headerTitles = [];
  final ScrollController sc = ScrollController();
  late ValueNotifier<int> _curr;
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    loadData(context);
  }

  loadData(BuildContext context) {
    final homeProvider = context.read<HomeProvider>();

    if (homeProvider.homeData != null) {
      tabController = TabController(vsync: this, length: homeProvider.headerTitles.length, initialIndex: 1);
      tabController.addListener(() {
        _curr.value = tabController.index;
        log("PREV:${_curr.value}: ${tabController.previousIndex}");
      });
    }
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _curr = useState<int>(1);

    return Scaffold(
      backgroundColor: AppColors.lightPurpleColor,
      floatingActionButton: FloatingActionButton(
        heroTag: "f1",
        onPressed: () {},
        backgroundColor: AppColors.whiteColor,
        child: const Icon(
          Icons.add,
          color: AppColors.blackColor,
        ),
      ),
      body: GestureDetector(
        onVerticalDragEnd: (dragEndDetails) async {
          final homeProvider = context.read<HomeProvider>();
          if (dragEndDetails.primaryVelocity != null) {
            if (dragEndDetails.primaryVelocity! < 0) {
              log("THIS IS TEH SWIPE UP: ${dragEndDetails.primaryVelocity}");
            }
            if (dragEndDetails.primaryVelocity! > 0) {
              log("THIS IS TEH SWIPE DOWN: ${dragEndDetails.primaryVelocity}");
              final result = await Utilities.openActivity(
                context,
                MenuScreen(
                  headerTitles: homeProvider.headerTitles,
                  currentValue: _curr.value,
                ),
                transitionDuration: 600,
                transitionBuilder: (context, animation, secondaryAnimation, child) {
                  const begin = Offset(0.0, -1.0);
                  const end = Offset.zero;
                  final tween = Tween(begin: begin, end: end);
                  final offsetAnimation = animation.drive(tween);

                  return SlideTransition(
                    position: offsetAnimation,
                    child: child,
                  );
                },
              );
              if (result != null) {
                log("$result");
              }
            }
          }
        },
        child: NotificationListener(onNotification: (scrollNotification) {
          final homeProvider = context.read<HomeProvider>();
          if (scrollNotification is ScrollUpdateNotification) {
            if (scrollNotification.dragDetails != null) {
              if (scrollNotification.dragDetails!.delta.dx < 0) {
                homeProvider.setScrollLeft(false);
              }
              if (scrollNotification.dragDetails!.delta.dx > 0) {
                homeProvider.setScrollLeft(true);
              }
            }
          }
          return true;
        }, child: Consumer<HomeProvider>(
          builder: (context, homeProvider, _) {
            return Stack(
              children: [
                TabBarView(
                  controller: tabController,
                  children: homeScreenTabList,
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: Utilities.screenHeight(context) * 0.2,
                    width: Utilities.screenWidth(context),
                    decoration: const BoxDecoration(
                      color: AppColors.whiteColor,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(80),
                      ),
                    ),
                    padding: EdgeInsets.only(left: 30, top: MediaQuery.of(context).padding.top + 20),
                    child: TabBar(
                        controller: tabController,
                        physics: const BouncingScrollPhysics(),
                        isScrollable: true,
                        labelColor: Colors.black,
                        unselectedLabelColor: AppColors.greyColor,
                        indicatorSize: TabBarIndicatorSize.label,
                        labelPadding: const EdgeInsets.only(left: 30, right: 30),
                        indicator: BoxDecoration(color: AppColors.transparent),
                        enableFeedback: false,
                        tabs: homeProvider.headerTitles
                            .map<Widget>(
                              (item) => Hero(
                                tag: item,
                                flightShuttleBuilder: (
                                  BuildContext flightContext,
                                  Animation<double> animation,
                                  HeroFlightDirection flightDirection,
                                  BuildContext fromHeroContext,
                                  BuildContext toHeroContext,
                                ) {
                                  return SingleChildScrollView(
                                    child: fromHeroContext.widget,
                                  );
                                },
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: item == "you"
                                      ? Material(
                                          type: MaterialType.transparency,
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 60,
                                                width: 60,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        color: _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                            ? AppColors.pinkColor
                                                            : AppColors.greyColorShadow,
                                                        width: _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                            ? 2.5
                                                            : 1)),
                                                child: ClipRRect(
                                                  borderRadius: BorderRadius.circular(100),
                                                  child: Image.network(
                                                    "https://fastly.picsum.photos/id/237/536/354.jpg?hmac=i0yVXW1ORpyCZpQ-CknuyV-jbtU7_x9EBQVhvT5aRr0",
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ).animate().fade(duration: const Duration(milliseconds: 350)),
                                              const SizedBox(height: 10),
                                              Text("YOU",
                                                  style: TextStyle(
                                                      fontWeight: item == "you" ? semibold : medium, fontSize: 12))
                                            ],
                                          ),
                                        )
                                      : Material(
                                          type: MaterialType.transparency,
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 60,
                                                width: 60,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                    color: _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                        ? item == "trending"
                                                            ? AppColors.pinkColor
                                                            : item == "health"
                                                                ? AppColors.lightGreenColor
                                                                : item == "music"
                                                                    ? AppColors.skyBlueColor
                                                                    : AppColors.greyColorShadow
                                                        : AppColors.greyColorShadow,
                                                    width: _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                        ? 2.5
                                                        : 1,
                                                  ),
                                                ),
                                                child: Center(
                                                  child: item == "trending"
                                                      ? Icon(Icons.trending_up,
                                                          size: 28,
                                                          color:
                                                              _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                                  ? AppColors.darkBlueColor
                                                                  : AppColors.fadedBlueColor)
                                                      : item == "health"
                                                          ? Icon(Icons.favorite_outline,
                                                              size: 28,
                                                              color: _curr.value ==
                                                                      (homeProvider.headerTitles.indexOf(item))
                                                                  ? AppColors.darkBlueColor
                                                                  : AppColors.fadedBlueColor)
                                                          : item == "music"
                                                              ? Icon(Icons.headphones,
                                                                  size: 28,
                                                                  color: _curr.value ==
                                                                          (homeProvider.headerTitles.indexOf(item))
                                                                      ? AppColors.darkBlueColor
                                                                      : AppColors.fadedBlueColor)
                                                              : item == "sports"
                                                                  ? Icon(Icons.sports_basketball_outlined,
                                                                      size: 28,
                                                                      color: _curr.value ==
                                                                              (homeProvider.headerTitles.indexOf(item))
                                                                          ? AppColors.darkBlueColor
                                                                          : AppColors.fadedBlueColor)
                                                                  : Icon(Icons.chrome_reader_mode_outlined,
                                                                          size: 28,
                                                                          color: _curr.value ==
                                                                                  (homeProvider.headerTitles
                                                                                      .indexOf(item))
                                                                              ? AppColors.darkBlueColor
                                                                              : AppColors.fadedBlueColor)
                                                                      .animate()
                                                                      .shake(),
                                                )
                                                    .animate(
                                                      delay: 250.ms,
                                                      onPlay: (controller) =>
                                                          _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                              ? controller.animateBack(1)
                                                              : controller.stop(),
                                                    )
                                                    .shake(),
                                              ),
                                              const SizedBox(height: 10),
                                              Text(item.toUpperCase(),
                                                  style: TextStyle(
                                                      fontWeight:
                                                          _curr.value == (homeProvider.headerTitles.indexOf(item))
                                                              ? semibold
                                                              : medium,
                                                      fontSize: 12)),
                                              const Spacer(),
                                            ],
                                          ),
                                        ),
                                ),
                              ),
                            )
                            .toList()),
                  ).animate().slideY(curve: Curves.decelerate, delay: const Duration(milliseconds: 0)),
                ),
              ],
            );
          },
        )),
      ),
    );
  }
}
