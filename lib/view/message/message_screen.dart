import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/message_widgets/message_list_item_widget.dart';
import 'package:bottle_demo_ui/res/components/title_widget.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:provider/provider.dart';

class MessageScreen extends StatefulWidget {
  const MessageScreen({super.key});

  @override
  State<MessageScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<MessageScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.darkPurpleColor,
      body: SizedBox(
          height: Utilities.screenHeight(context),
          width: Utilities.screenWidth(context),
          child: Consumer<HomeProvider>(
            builder: (context, homeProvider, _) {
              final messageData = homeProvider.messageData?.response.data;
              return Column(
                children: [
                  Container(
                    height: 180,
                    width: Utilities.screenWidth(context),
                    decoration: const BoxDecoration(
                        color: AppColors.whiteColor, borderRadius: BorderRadius.only(bottomLeft: Radius.circular(80))),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 40),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 10),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                BackButton(
                                  onPressed: () {
                                    Utilities.closeActivity(context);
                                  },
                                  color: AppColors.lightPurpleAccentColor,
                                ),
                                IconButton(
                                  onPressed: () {},
                                  icon: const Icon(
                                    Icons.search,
                                    color: AppColors.lightPurpleAccentColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(height: 5),
                          const Center(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 40),
                              child: Text(
                                "Messages",
                                style: TextStyle(fontSize: 26, fontWeight: bold, color: AppColors.darkPurpleColor),
                                softWrap: true,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ],
                      ).animate().fade().slideY(
                            duration: 500.ms,
                            curve: Curves.decelerate,
                            begin: -1,
                            end: 0,
                          ),
                    ),
                  ),
                  Expanded(
                    child: messageData == null
                        ? const Center(child: TitleWidget(title: "No Data"))
                        : CustomScrollView(
                            physics: const BouncingScrollPhysics(),
                            slivers: [
                              SliverPadding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                sliver: SliverList(
                                  delegate: SliverChildBuilderDelegate(
                                    (context, index) {
                                      return MessageListItemWidget(
                                        messageData: messageData[index],
                                        onPressed: (messageData) {},
                                      ).animate().slideX(
                                            duration: Duration(milliseconds: (index + 1) * 40),
                                            curve: Curves.decelerate,
                                            begin: 1,
                                            end: 0,
                                          );
                                    },
                                    childCount: messageData.length,
                                  ),
                                ),
                              ),
                            ],
                          ),
                  ),
                ],
              );
            },
          )),
    );
  }
}
