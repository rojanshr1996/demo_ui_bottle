import 'dart:developer';

import 'package:bottle_demo_ui/model/detail_model.dart';
import 'package:bottle_demo_ui/model/home_model.dart';
import 'package:bottle_demo_ui/model/messages_model.dart';
import 'package:bottle_demo_ui/model/profile_model.dart';
import 'package:bottle_demo_ui/utils/load_resource.dart';
import 'package:flutter/material.dart';

class HomeProvider with ChangeNotifier {
  HomeModel? _homeData;
  DetailModel? _detailData;
  ProfileModel? _profileData;
  MessageModel? _messageData;
  List<String> _headerTitles = [];
  bool _isLoading = true;
  bool _isDetailLoading = true;
  bool _onChecked = false;
  bool _scrollLeft = false;

  List<String> get headerTitles => _headerTitles;
  setHeaderTiles(List<String> data) {
    _headerTitles = data;
    notifyListeners();
  }

  bool get scrollLeft => _scrollLeft;
  setScrollLeft(bool data) {
    _scrollLeft = data;
    notifyListeners();
  }

  bool get onChecked => _onChecked;
  setOnChecked(bool data) {
    _onChecked = data;
    notifyListeners();
  }

  bool get isLoading => _isLoading;
  setLoading(bool data) {
    _isLoading = data;
    notifyListeners();
  }

  bool get isDetailLoading => _isDetailLoading;
  setDetailLoading(bool data) {
    _isDetailLoading = data;
    notifyListeners();
  }

  HomeModel? get homeData => _homeData;
  setHomeData(HomeModel? data) {
    _homeData = data;
    notifyListeners();
  }

  DetailModel? get detailData => _detailData;
  setDetailData(DetailModel? data) {
    _detailData = data;
    notifyListeners();
  }

  ProfileModel? get profileData => _profileData;
  setProfileData(ProfileModel? data) {
    _profileData = data;
    notifyListeners();
  }

  MessageModel? get messageData => _messageData;
  setMessageData(MessageModel? data) {
    _messageData = data;
    notifyListeners();
  }

  getHomeData() async {
    setLoading(true);
    try {
      final result = await loadHomeResource();

      if (result != null) {
        final homeData = HomeModel.fromJson(result);
        log("THIS IS THE RESULT: $homeData");
        List<String> headerTitles = homeData.response.toJson().keys.toList();
        headerTitles.insert(0, "you");
        setHeaderTiles(headerTitles);
        setHomeData(homeData);
        setLoading(false);
        return homeData;
      }
    } catch (e) {
      setDetailLoading(false);
      log("THIS IS THE RESULT: $e");
    }
  }

  getDetailData() async {
    setDetailLoading(true);
    try {
      final result = await loadDetailResource();

      if (result != null) {
        final detailData = DetailModel.fromJson(result);
        log("THIS IS THE DETAIL RESULT: $detailData");
        setDetailLoading(false);
        setDetailData(detailData);
        return detailData;
      }
    } catch (e) {
      setDetailLoading(false);
      log("THIS IS THE RESULT: $e");
    }
  }

  getProfileData() async {
    setDetailLoading(true);
    try {
      final result = await loadProfileResource();

      if (result != null) {
        final profileData = ProfileModel.fromJson(result);
        log("THIS IS THE PROFILE RESULT: $profileData");
        setDetailLoading(false);
        setProfileData(profileData);
        return profileData;
      }
    } catch (e) {
      setDetailLoading(false);
      log("THIS IS THE RESULT: $e");
    }
  }

  getMessageData() async {
    setDetailLoading(true);
    try {
      final result = await loadMessageResource();
      if (result != null) {
        final messageData = MessageModel.fromJson(result);
        log("THIS IS THE Message RESULT: $messageData");
        setDetailLoading(false);
        setMessageData(messageData);
        return profileData;
      }
    } catch (e) {
      setDetailLoading(false);
      log("THIS IS THE RESULT: $e");
    }
  }
}
