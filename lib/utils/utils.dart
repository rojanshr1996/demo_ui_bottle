import 'package:bottle_demo_ui/model/home_model.dart';
import 'package:bottle_demo_ui/res/components/stacked_widget.dart';
import 'package:flutter/material.dart';

class Utils {
  Utils._();

  static Widget buildStackedImages(
      {required List<Member> membersList, int length = 2, double xShift = 10, double size = 26}) {
    List<String> urlImages = [];
    if (membersList.isNotEmpty) {
      for (Member element in membersList) {
        urlImages.add(element.image);
        if (urlImages.length == length) {
          break;
        }
      }
    }

    final items = urlImages.map((urlImage) => buildImage(urlImage)).toList();

    return StackedWidgets(
      items: items,
      size: size,
      xShift: xShift,
    );
  }

  static Widget buildImage(String urlImage) {
    const double borderSize = 1.5;

    return ClipOval(
      child: Container(
        padding: const EdgeInsets.all(borderSize),
        color: Colors.white,
        child: ClipOval(
          child: Image.network(
            urlImage,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
