import 'dart:convert';
import 'package:flutter/services.dart';

loadDetailResource() async {
  final String response = await rootBundle.loadString('assets/data/detail_dummy_data.json');
  final data = await json.decode(response);
  return data;
}

loadHomeResource() async {
  final String response = await rootBundle.loadString('assets/data/home_dummy_data.json');
  final data = await json.decode(response);
  return data;
}

loadProfileResource() async {
  final String response = await rootBundle.loadString('assets/data/profile_dummy_data.json');
  final data = await json.decode(response);
  return data;
}

loadMessageResource() async {
  final String response = await rootBundle.loadString('assets/data/messages_dummy_data.json');
  final data = await json.decode(response);
  return data;
}
