// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

ProfileModel welcomeFromJson(String str) => ProfileModel.fromJson(json.decode(str));

String welcomeToJson(ProfileModel data) => json.encode(data.toJson());

class ProfileModel {
  ProfileModel({
    required this.response,
  });

  Response response;

  factory ProfileModel.fromJson(Map<String, dynamic> json) => ProfileModel(
        response: Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "response": response.toJson(),
      };
}

class Response {
  Response({
    required this.name,
    required this.totalMeetups,
    required this.image,
    required this.aboutMe,
    required this.timeline,
  });

  String name;
  int totalMeetups;
  String image;
  String aboutMe;
  String timeline;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        name: json["name"],
        totalMeetups: json["totalMeetups"],
        image: json["image"],
        aboutMe: json["aboutMe"],
        timeline: json["timeline"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "totalMeetups": totalMeetups,
        "image": image,
        "aboutMe": aboutMe,
        "timeline": timeline,
      };
}
