// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

// ignore_for_file: constant_identifier_names

import 'dart:convert';

MessageModel welcomeFromJson(String str) => MessageModel.fromJson(json.decode(str));

String welcomeToJson(MessageModel data) => json.encode(data.toJson());

class MessageModel {
  MessageModel({
    required this.response,
  });

  Response response;

  factory MessageModel.fromJson(Map<String, dynamic> json) => MessageModel(
        response: Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "response": response.toJson(),
      };
}

class Response {
  Response({
    required this.data,
  });

  List<Datum> data;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    required this.name,
    required this.image,
    required this.date,
    required this.message,
  });

  Name name;
  String image;
  DateTime date;
  Message message;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        name: nameValues.map[json["name"]]!,
        image: json["image"],
        date: DateTime.parse(json["date"]),
        message: messageValues.map[json["message"]]!,
      );

  Map<String, dynamic> toJson() => {
        "name": nameValues.reverse[name],
        "image": image,
        "date": date.toIso8601String(),
        "message": messageValues.reverse[message],
      };
}

enum Message { CHECKOUT_THIS_MESSAGE }

final messageValues = EnumValues({"Checkout this message": Message.CHECKOUT_THIS_MESSAGE});

enum Name { JANE_DOE, JOHN_DOE }

final nameValues = EnumValues({"Jane Doe": Name.JANE_DOE, "John Doe": Name.JOHN_DOE});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
