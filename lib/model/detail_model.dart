// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

import 'package:bottle_demo_ui/model/home_model.dart';

DetailModel welcomeFromJson(String str) => DetailModel.fromJson(json.decode(str));

String welcomeToJson(DetailModel data) => json.encode(data.toJson());

class DetailModel {
  DetailModel({
    required this.response,
  });

  Response response;

  factory DetailModel.fromJson(Map<String, dynamic> json) => DetailModel(
        response: Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "response": response.toJson(),
      };
}

class Response {
  Response({
    required this.date,
    required this.title,
    required this.repeat,
    required this.place,
    required this.members,
    required this.cost,
    required this.location,
    required this.address,
    required this.host,
    required this.description,
  });

  DateTime date;
  String title;
  String repeat;
  Place place;
  List<Member> members;
  String cost;
  String location;
  String address;
  String host;
  String description;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        date: DateTime.parse(json["date"]),
        title: json["title"],
        repeat: json["repeat"],
        place: Place.fromJson(json["place"]),
        members: List<Member>.from(json["members"].map((x) => Member.fromJson(x))),
        cost: json["cost"],
        location: json["location"],
        address: json["address"],
        host: json["host"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "date": date.toIso8601String(),
        "title": title,
        "repeat": repeat,
        "place": place.toJson(),
        "members": List<dynamic>.from(members.map((x) => x.toJson())),
        "cost": cost,
        "location": location,
        "address": address,
        "host": host,
        "description": description,
      };
}

class Place {
  Place({
    required this.name,
    required this.location,
    required this.image,
  });

  String name;
  String location;
  String image;

  factory Place.fromJson(Map<String, dynamic> json) => Place(
        name: json["name"],
        location: json["location"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "location": location,
        "image": image,
      };
}
