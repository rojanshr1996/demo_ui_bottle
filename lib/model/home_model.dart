// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

HomeModel welcomeFromJson(String str) => HomeModel.fromJson(json.decode(str));

String welcomeToJson(HomeModel data) => json.encode(data.toJson());

class HomeModel {
  HomeModel({
    required this.response,
  });

  Response response;

  factory HomeModel.fromJson(Map<String, dynamic> json) => HomeModel(
        response: Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "response": response.toJson(),
      };
}

class Response {
  Response({
    required this.trending,
    required this.health,
    required this.music,
    required this.sports,
    required this.learning,
  });

  Health trending;
  Health health;
  Health music;
  Health sports;
  Health learning;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        trending: Health.fromJson(json["trending"]),
        health: Health.fromJson(json["health"]),
        music: Health.fromJson(json["music"]),
        sports: Health.fromJson(json["sports"]),
        learning: Health.fromJson(json["learning"]),
      );

  Map<String, dynamic> toJson() => {
        "trending": trending.toJson(),
        "health": health.toJson(),
        "music": music.toJson(),
        "sports": sports.toJson(),
        "learning": learning.toJson(),
      };
}

class Health {
  Health({
    required this.colorScheme,
    required this.data,
  });

  String colorScheme;
  List<Datum> data;

  factory Health.fromJson(Map<String, dynamic> json) => Health(
        colorScheme: json["colorScheme"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "colorScheme": colorScheme,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    required this.date,
    required this.title,
    required this.members,
  });

  DateTime date;
  String title;
  List<Member> members;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        date: DateTime.parse(json["date"]),
        title: json["title"],
        members: List<Member>.from(json["members"].map((x) => Member.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "date": date.toIso8601String(),
        "title": title,
        "members": List<dynamic>.from(members.map((x) => x.toJson())),
      };
}

class Member {
  Member({
    required this.image,
    required this.name,
  });

  String image;
  Name name;

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        image: json["image"],
        name: nameValues.map[json["name"]]!,
      );

  Map<String, dynamic> toJson() => {
        "image": image,
        "name": nameValues.reverse[name],
      };
}

// ignore: constant_identifier_names
enum Name { JANE_DOE, JOHN_DOE }

final nameValues = EnumValues({"Jane Doe": Name.JANE_DOE, "John Doe": Name.JOHN_DOE});

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    reverseMap = map.map((k, v) => MapEntry(v, k));
    return reverseMap;
  }
}
