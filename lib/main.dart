import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/route/app_router.dart';
import 'package:bottle_demo_ui/res/route/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => HomeProvider()),
      ],
      child: MaterialApp(
        title: 'Bottle Test UI',
        debugShowCheckedModeBanner: false,
        onGenerateRoute: AppRouter.onGenerateRoute,
        theme: ThemeData(fontFamily: 'montserrat'),
        initialRoute: Routes.splash,
      ),
    );
  }
}
