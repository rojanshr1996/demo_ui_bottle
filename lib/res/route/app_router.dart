import 'package:bottle_demo_ui/model/home_model.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/title_widget.dart';
import 'package:bottle_demo_ui/res/route/routes.dart';
import 'package:bottle_demo_ui/view/home_screen.dart';
import 'package:bottle_demo_ui/view/menu/menu_screen.dart';
import 'package:bottle_demo_ui/view/message/message_screen.dart';
import 'package:bottle_demo_ui/view/splash_screen.dart';
import 'package:bottle_demo_ui/view/trending/trending_detail_screen.dart';
import 'package:flutter/material.dart';

class AppRouter {
  AppRouter._();
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.splash:
        return MaterialPageRoute(builder: (_) => const SplashScreen());

      case Routes.home:
        return MaterialPageRoute(builder: (_) => const HomeScreen());

      case Routes.detail:
        if (args is Datum) {
          return MaterialPageRoute(
            builder: (_) => TrendingDetailScreen(trendingData: args),
          );
        }
        return errorRoute(settings);

      case Routes.message:
        return MaterialPageRoute(builder: (_) => const MessageScreen());

      case Routes.menu:
        if (args is List<String>) {
          return MaterialPageRoute(
            builder: (_) => MenuScreen(headerTitles: args),
          );
        }
        return errorRoute(settings);

      default:
        return errorRoute(settings);
    }
  }

  static Route<dynamic> errorRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (_) => Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
        ),
        body: Center(
            child: TitleWidget(
          title: 'No Route defined for ${settings.name}',
          textStyle: const TextStyle(
            color: AppColors.darkPurpleColor,
            fontSize: 16,
          ),
        )),
      ),
    );
  }
}
