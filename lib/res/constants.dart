import 'package:bottle_demo_ui/view/tabs/health_tab.dart';
import 'package:bottle_demo_ui/view/tabs/learning_tab.dart';
import 'package:bottle_demo_ui/view/tabs/music_tab.dart';
import 'package:bottle_demo_ui/view/tabs/profile_tab.dart';
import 'package:bottle_demo_ui/view/tabs/sports_tab.dart';
import 'package:bottle_demo_ui/view/tabs/trending_tab.dart';
import 'package:flutter/material.dart';

const FontWeight light = FontWeight.w300;
const FontWeight regular = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semibold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
const FontWeight extrabold = FontWeight.w800;
const FontWeight black = FontWeight.w900;

final List<Widget> homeScreenTabList = <Widget>[
  const ProfileTab(),
  const TrendingTab(),
  const HealthTab(),
  const MusicTab(),
  const SportsTab(),
  const LearningTab(),
];
