import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color blackColor = Color(0xff000000);

  static const Color pinkColor = Color(0xffD47FA6);

  static const Color purpleColor = Color(0xff5F4591);
  static const Color lightPurpleColor = Color(0xff9599B3);
  static const Color lightPurpleAccentColor = Color(0xff998FA2);
  static const Color purpleAccent = Color(0xff8A56AC);
  static const Color deepPurpleColor = Color(0xff352641);
  static const Color darkPurpleColor = Color(0xff241332);
  static Color lightPurpleAccentColorShadow = lightPurpleAccentColor.withOpacity(0.5);

  static const Color lightColor = Color(0xffF1F0F2);
  static const Color whiteColor = Color(0xffffffff);

  static const Color greenColor = Color(0xff417623);
  static const Color greenAccentColor = Color(0xff52912E);
  static const Color lightGreenColor = Color(0xffB4C55B);
  static const Color darkGreenColor = Color(0xff253E12);

  static const Color darkBlueColor = Color(0xff132641);
  static const Color skyBlueColor = Color(0xff4EBDEF);
  static const Color blueColor = Color(0xff4666E5);
  static const Color blueAccentColor = Color(0xff4F8DCB);
  static const Color fadedBlueColor = Color(0xff9AA6AC);

  static const Color greyColor = Color(0xff979797);
  static Color greyColorShadow = greyColor.withOpacity(0.3);

  static Color transparent = Colors.transparent;

  static LinearGradient lightPurpleGradientPicture = LinearGradient(
      stops: const [0, 1],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [AppColors.transparent, AppColors.deepPurpleColor.withOpacity(0.6)]);
}
