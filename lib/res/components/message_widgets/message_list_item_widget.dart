import 'package:bottle_demo_ui/model/messages_model.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/title_widget.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

typedef MessageCallBack = Function(Datum data);

class MessageListItemWidget extends StatelessWidget {
  final Datum messageData;
  final MessageCallBack onPressed;

  const MessageListItemWidget({
    Key? key,
    required this.messageData,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.darkPurpleColor,
      shadowColor: AppColors.lightPurpleAccentColor,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 30),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.network(
                height: 45,
                messageData.image,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TitleWidget(
                    title: messageData.name.name,
                    textAlign: TextAlign.start,
                    textStyle: const TextStyle(
                      fontSize: 12,
                      fontWeight: semibold,
                      color: AppColors.lightPurpleAccentColor,
                    ),
                  ),
                  const SizedBox(height: 10),
                  TitleWidget(
                    title: messageData.message.name,
                    textAlign: TextAlign.start,
                    textStyle: const TextStyle(
                      fontWeight: bold,
                      fontSize: 14,
                      color: AppColors.whiteColor,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 20),
            TitleWidget(
              title: DateFormat.MMMd().format(messageData.date),
              textAlign: TextAlign.start,
              textStyle: const TextStyle(
                fontSize: 12,
                fontWeight: semibold,
                color: AppColors.lightPurpleAccentColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
