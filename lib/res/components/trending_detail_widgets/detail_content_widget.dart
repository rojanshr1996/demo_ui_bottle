import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

class DetailContentWidget extends StatelessWidget {
  final IconData? iconData;

  final List<Widget> content;

  const DetailContentWidget({
    super.key,
    this.iconData,
    this.content = const [],
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          iconData == null
              ? const SizedBox.shrink()
              : Icon(
                  iconData,
                  color: AppColors.lightPurpleAccentColorShadow,
                ),
          iconData == null ? const SizedBox.shrink() : const SizedBox(width: 15),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: content,
            ),
          )
        ],
      ),
    )
        .animate()
        .fade(
          delay: const Duration(milliseconds: 100),
          duration: const Duration(milliseconds: 500),
        )
        .slideY(
          duration: const Duration(milliseconds: 500),
          curve: Curves.decelerate,
        );
  }
}
