import 'dart:developer';

import 'package:bottle_demo_ui/model/detail_model.dart';
import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/rounded_icon_widget.dart';
import 'package:bottle_demo_ui/res/components/title_widget.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:like_button/like_button.dart';
import 'package:provider/provider.dart';

class DetailStackWidget extends StatefulWidget {
  final Response detailData;
  final Function(bool)? onAccept;

  const DetailStackWidget({super.key, required this.detailData, this.onAccept});

  @override
  State<DetailStackWidget> createState() => _DetailStackWidgetState();
}

class _DetailStackWidgetState extends State<DetailStackWidget> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _colorTween;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 750));
    // _colorTween = ColorTween(begin: AppColors.darkPurpleColor, end: AppColors.whiteColor).animate(_animationController);
    changeColor(context);
    super.initState();
  }

  changeColor(BuildContext context) {
    HomeProvider homeProvider = Provider.of<HomeProvider>(context, listen: false);
    log("${homeProvider.onChecked}");
    if (homeProvider.onChecked) {
      _colorTween =
          ColorTween(begin: AppColors.whiteColor, end: AppColors.darkPurpleColor).animate(_animationController);
    } else {
      _colorTween =
          ColorTween(begin: AppColors.darkPurpleColor, end: AppColors.whiteColor).animate(_animationController);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    HomeProvider homeProvider = context.watch<HomeProvider>();

    return Container(
      width: Utilities.screenWidth(context),
      decoration: const BoxDecoration(
          color: AppColors.whiteColor, borderRadius: BorderRadius.only(bottomLeft: Radius.circular(80))),
      child: Padding(
        padding: EdgeInsets.fromLTRB(40, Utilities.screenHeight(context) * 0.3 + 30, 40, 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Image.network(
                    height: 36,
                    widget.detailData.place.image,
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(width: 15),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWidget(
                        title: widget.detailData.place.name,
                        textAlign: TextAlign.start,
                        textStyle: const TextStyle(
                          fontWeight: bold,
                          color: AppColors.darkPurpleColor,
                        ),
                      ),
                      const SizedBox(height: 4),
                      TitleWidget(
                        title: widget.detailData.place.name,
                        textAlign: TextAlign.start,
                        textStyle: const TextStyle(
                          fontWeight: medium,
                          fontSize: 12,
                          color: AppColors.lightPurpleAccentColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 30),
            AnimatedBuilder(
              animation: _colorTween,
              builder: (context, child) => Container(
                decoration: BoxDecoration(
                  // color: homeProvider.onChecked ? AppColors.whiteColor : AppColors.darkPurpleColor,
                  color: _colorTween.value,
                  border: Border.all(
                      color: homeProvider.onChecked ? AppColors.lightPurpleAccentColorShadow : AppColors.transparent),
                  borderRadius: BorderRadius.circular(40),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.0),
                        child: Icon(
                          Icons.edit,
                          color: AppColors.lightPurpleAccentColor,
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const TitleWidget(
                              title: "Are you going?",
                              textAlign: TextAlign.start,
                              textStyle: TextStyle(
                                fontWeight: bold,
                                fontSize: 12,
                                color: AppColors.whiteColor,
                              ),
                            )
                                .animate(
                                    onPlay: (controller) =>
                                        homeProvider.onChecked ? controller.animateBack(1) : controller.stop())
                                .swap(
                                  duration: 150.milliseconds,
                                  builder: (_, __) => const TitleWidget(
                                    title: "You are going",
                                    textAlign: TextAlign.start,
                                    textStyle: TextStyle(
                                      fontWeight: bold,
                                      fontSize: 12,
                                      color: AppColors.darkPurpleColor,
                                    ),
                                  ),
                                ),
                            const SizedBox(height: 4),
                            TitleWidget(
                              title: "56 spots left",
                              textAlign: TextAlign.start,
                              textStyle: TextStyle(
                                fontWeight: medium,
                                fontSize: 10,
                                color: AppColors.lightPurpleAccentColorShadow,
                              ),
                            ),
                          ],
                        ),
                      ),
                      // homeProvider.onChecked
                      //     ? const SizedBox.shrink()
                      //     :
                      RoundedIconWidget(
                        iconData: Icons.close,
                        backgroundColor: AppColors.lightPurpleAccentColorShadow,
                      )
                          .animate(
                              onPlay: (controller) =>
                                  homeProvider.onChecked ? controller.animateBack(1) : controller.stop())
                          .rotate(
                            delay: 150.ms,
                            duration: 250.ms,
                          )
                          .slideX(
                            delay: 150.ms,
                            begin: 0,
                            end: -0.1,
                          )
                          .fadeOut(
                            delay: 150.ms,
                            duration: 250.ms,
                          ),
                      const SizedBox(width: 10),
                      LikeButton(
                        size: 40,
                        bubblesColor: const BubblesColor(
                          dotPrimaryColor: AppColors.deepPurpleColor,
                          dotSecondaryColor: AppColors.blueAccentColor,
                        ),
                        circleColor:
                            const CircleColor(start: AppColors.blueAccentColor, end: AppColors.deepPurpleColor),
                        isLiked: homeProvider.onChecked,
                        likeBuilder: (bool isLiked) {
                          return RoundedIconWidget(
                            iconData: Icons.check,
                            backgroundColor: homeProvider.onChecked
                                ? AppColors.blueAccentColor
                                : AppColors.lightPurpleAccentColorShadow,
                          );
                        },
                        onTap: (isLiked) async {
                          if (_animationController.status == AnimationStatus.completed) {
                            _animationController.reverse();
                          } else {
                            _animationController.forward();
                          }
                          return widget.onAccept!(isLiked);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ).animate().slideY(
        duration: const Duration(milliseconds: 500),
        curve: Curves.decelerate,
        delay: const Duration(milliseconds: 100));
  }
}
