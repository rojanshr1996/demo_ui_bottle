import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:flutter/cupertino.dart';

class TitleWidget extends StatelessWidget {
  final String title;
  final TextStyle? textStyle;
  final TextAlign? textAlign;

  const TitleWidget({
    Key? key,
    required this.title,
    this.textStyle,
    this.textAlign = TextAlign.center,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: textStyle ??
          const TextStyle(
            fontSize: 16,
            fontWeight: bold,
            color: AppColors.whiteColor,
          ),
      textAlign: textAlign,
    );
  }
}

class RefreshTitleWidget extends StatelessWidget {
  final String title;
  final TextStyle? textStyle;
  final Future<void> Function() onRefresh;

  const RefreshTitleWidget({
    Key? key,
    required this.title,
    this.textStyle,
    required this.onRefresh,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      slivers: <Widget>[
        CupertinoSliverRefreshControl(onRefresh: onRefresh, refreshTriggerPullDistance: 160),
        SliverFillRemaining(
          child: TitleWidget(
            title: title,
            textStyle: textStyle ??
                const TextStyle(
                  fontSize: 16,
                  fontWeight: bold,
                  color: AppColors.whiteColor,
                ),
          ),
        ),
      ],
    );
  }
}
