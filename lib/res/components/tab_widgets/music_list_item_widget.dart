import 'package:bottle_demo_ui/model/home_model.dart';
import 'package:bottle_demo_ui/providers/home_provider.dart';
import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:bottle_demo_ui/utils/utilities.dart';
import 'package:bottle_demo_ui/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

typedef MusicCallBack = Function(Datum data);

class MusicListItemWidget extends StatelessWidget {
  final List<Datum> musicList;
  final List<Color> backgroundColor;
  final MusicCallBack onPressed;

  const MusicListItemWidget({
    Key? key,
    required this.musicList,
    required this.backgroundColor,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeProvider = context.watch<HomeProvider>();

    final allItems = musicList
        .toList()
        .asMap()
        .map((index, musicData) {
          List<String> names = [];
          if (musicData.members.isNotEmpty) {
            for (Member element in musicData.members) {
              names.add(element.name.name);
            }
          }
          final value = Positioned(
            top: index * Utilities.screenHeight(context) * 0.25,
            child: GestureDetector(
              onTap: () {
                onPressed(musicData);
              },
              child: Container(
                width: Utilities.screenWidth(context),
                decoration: BoxDecoration(
                    color: backgroundColor[index],
                    borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(80))),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(40, Utilities.screenHeight(context) * 0.2 + 40, 40, 40),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${DateFormat.yMMMEd().format(musicData.date)}, ${DateFormat.jm().format(musicData.date)}",
                        style: TextStyle(
                          color: AppColors.whiteColor.withOpacity(0.5),
                          fontSize: 12,
                          fontWeight: semibold,
                        ),
                        softWrap: true,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      const SizedBox(height: 5),
                      Text(
                        musicData.title,
                        style: const TextStyle(fontSize: 24, fontWeight: bold, color: AppColors.whiteColor),
                        softWrap: true,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      const SizedBox(height: 15),
                      names.isEmpty
                          ? const SizedBox.shrink()
                          : Row(
                              children: [
                                Utils.buildStackedImages(membersList: musicData.members),
                                const SizedBox(width: 10),
                                Text(
                                  "${names[1]}, ${names[2]} & ${names.length - 2} others",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: medium,
                                    color: AppColors.whiteColor.withOpacity(0.5),
                                  ),
                                ),
                              ],
                            )
                    ],
                  ),
                ),
              ),
            ),
          ).animate().slideX(
                delay: Duration(milliseconds: (index + 1) * 100),
                curve: Curves.decelerate,
                begin: homeProvider.scrollLeft ? -1 : 1,
                end: 0,
              );

          return MapEntry(index, value);
        })
        .values
        .toList();

    return Stack(
      children: allItems.reversed.toList(),
    );
  }
}
