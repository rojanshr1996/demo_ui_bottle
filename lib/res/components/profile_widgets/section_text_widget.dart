import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/components/title_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

class SectionTextWidget extends StatelessWidget {
  final String title;
  final String content;
  const SectionTextWidget({super.key, required this.title, required this.content});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.darkPurpleColor,
      shadowColor: AppColors.lightPurpleAccentColor,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60))),
      child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 36.0, horizontal: 40.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: TitleWidget(
                      title: title,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  const Icon(
                    Icons.keyboard_arrow_down_rounded,
                    color: AppColors.whiteColor,
                  )
                ],
              ),
              const SizedBox(height: 15),
              Text(
                content,
                style: const TextStyle(color: AppColors.lightPurpleAccentColor),
              ),
            ],
          )),
    ).animate().fade().slideX(
          duration: const Duration(milliseconds: 300),
          curve: Curves.decelerate,
          begin: -1,
          end: 0,
          delay: const Duration(milliseconds: 50),
        );
  }
}
