import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:bottle_demo_ui/res/constants.dart';
import 'package:flutter/material.dart';

class NameWidget extends StatelessWidget {
  final String name;
  final int meetupCount;
  final TextStyle? nameStyle;
  final TextStyle? meetupStyle;
  const NameWidget({super.key, required this.name, required this.meetupCount, this.nameStyle, this.meetupStyle});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "$meetupCount meetups",
          style: nameStyle ??
              const TextStyle(
                fontSize: 14,
                fontWeight: semibold,
                color: AppColors.lightColor,
              ),
        ),
        const SizedBox(height: 8),
        Text(
          name,
          style: meetupStyle ??
              const TextStyle(
                fontSize: 24,
                fontWeight: bold,
                color: AppColors.whiteColor,
              ),
        )
      ],
    );
  }
}
