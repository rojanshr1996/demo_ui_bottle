import 'package:bottle_demo_ui/res/app_colors.dart';
import 'package:flutter/material.dart';

class RoundedIconWidget extends StatelessWidget {
  final double? size;
  final Color? iconColor;
  final Color? backgroundColor;
  final IconData iconData;
  final double? iconSize;
  final VoidCallback? onPressed;
  const RoundedIconWidget(
      {super.key,
      this.size,
      this.iconColor,
      this.backgroundColor,
      required this.iconData,
      this.onPressed,
      this.iconSize});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: size ?? 40,
        width: size ?? 40,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: backgroundColor ?? AppColors.darkPurpleColor,
        ),
        child: Center(
          child: Icon(
            iconData,
            color: iconColor ?? AppColors.whiteColor,
            size: iconSize ?? 20,
          ),
        ),
      ),
    );
  }
}
